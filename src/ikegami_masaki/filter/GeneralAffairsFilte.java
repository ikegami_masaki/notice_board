package ikegami_masaki.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ikegami_masaki.beans.User;


@WebFilter("/*")
public class GeneralAffairsFilte  implements Filter {
	// 制限をかけるURI
	private static final String[] FILTER_LIST = { "management",
												  "signup",
												  "userEdit",
												  "userDelete"};

	@Override
	public void doFilter(ServletRequest req,  ServletResponse res,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");
		List<String> messages = new ArrayList<String>();

		String nowURI = request.getRequestURI();

		for (int i=0; i<FILTER_LIST.length ; i++) {

			// リストのURIと同じものに制限をかける
			if (nowURI.matches(".*" + FILTER_LIST[i])) {

				// 総務部ではないユーザーに制限
				if (user == null ||  user.getPositionId() != 1) {
					messages.add("アクセス権がありません");
					session.setAttribute("errorMessages", messages);
					response.sendRedirect("./");
					return;
				}
			}
		}
		chain.doFilter(request, response); // サーブレットを実行
	}

	public void init(FilterConfig config) throws ServletException{}
    public void destroy(){}
}
