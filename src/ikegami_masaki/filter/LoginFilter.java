package ikegami_masaki.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebFilter("/*")
public class LoginFilter implements Filter {
	private static final String NOT_FILTER_URI = "login";
	private static final String CSS_URL = "css";

	@Override
	public void doFilter(ServletRequest req,  ServletResponse res,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		String nowURI = request.getRequestURI();

		// css に制限をはずす
		if (nowURI.matches(".*" + CSS_URL) == false) {
			// ログイン制限の処理
			if (nowURI.matches(".*" + NOT_FILTER_URI) == false) {
				if (session.getAttribute("loginUser") == null) {
					messages.add("ログインしてください");
					session.setAttribute("errorMessages", messages);
					response.sendRedirect("login");
					return;
				}
			}
		}
		chain.doFilter(request, response); // サーブレットを実行

	}

	public void init(FilterConfig config) throws ServletException{}
    public void destroy(){}
}
