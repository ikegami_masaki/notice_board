package ikegami_masaki.dao;

import static ikegami_masaki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import ikegami_masaki.beans.UserPost;
import ikegami_masaki.exception.SQLRuntimeException;

public class UserPostDao {

	// UserPostをリストで返す
	// すべてから検索
	public List<UserPost> getUserPost
				(Connection connection, String category, String startDate, String endDate) {

		PreparedStatement ps = null;
		try {
			// 投稿日時の降順に取得
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("posts.id as id,");
			sql.append("posts.user_id as user_id, ");
			sql.append("posts.text as text,");
			sql.append("posts.category as category, ");
			sql.append("posts.subject as subject ,");
			sql.append("posts.created_at as created_at, ");
			sql.append("users.name as name ");
			sql.append("FROM posts ");
			sql.append("INNER JOIN users ");
			sql.append("ON posts.user_id = users.id ");
			sql.append(" WHERE posts.created_at >= ? ");
			sql.append(" AND posts.created_at <= ? ");
			// 検索している場合
			if (category != null && category.isEmpty() == false) {
				sql.append(" AND posts.category  LIKE ?");
			}
			sql.append("ORDER BY created_at DESC");

			ps = connection.prepareStatement(sql.toString());

			// 検索の値設定
			ps.setString(1, startDate);
			ps.setString(2, endDate);
			if (category != null && category.isEmpty() == false) {
				ps.setString(3, "%" + category + "%" );
			}


			ResultSet rs = ps.executeQuery();
			List<UserPost> ret = toUserPostList(rs);

			return ret;

		}  catch (SQLException e) {
 			throw new SQLRuntimeException(e);
 		} finally {
 			close(ps);
 		}
	}

	// 検索結果からUserPostオブジェクトをリストで返す
	public List<UserPost> toUserPostList(ResultSet rs) throws SQLException {

		List<UserPost> ret = new ArrayList<UserPost>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String userName = rs.getString("name");
				int userId = rs.getInt("user_id");
				String text = rs.getString("text");
				String subject = rs.getString("subject");
				String category = rs.getString("category");
				Timestamp createdAt = rs.getTimestamp("created_at");

				UserPost userPost = new UserPost();

				userPost.setId(id);
				userPost.setUserName(userName);
				userPost.setUserId(userId);
				userPost.setText(text);
				userPost.setSubject(subject);
				userPost.setCategory(category);
				userPost.setCreatedAt(createdAt);

				ret.add(userPost);
			}

			return ret;
		} finally {
			close(rs);
		}
	}
}
