package ikegami_masaki.dao;

import static ikegami_masaki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ikegami_masaki.beans.User;
import ikegami_masaki.exception.NoRowsUpdatedRuntimeException;
import ikegami_masaki.exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			// sql文を作成
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("login_id");
			sql.append(",name");
			sql.append(", branch_id");
			sql.append(",position_id");
			sql.append(", is_deleted");
			sql.append(", password");
			sql.append(", created_at");
			sql.append(", updated_at");
			sql.append(") VALUES ( ");
			sql.append("?"); // 1  login_id
			sql.append(", ?");  // 2  name
			sql.append(", ?");  // 3  branch_id
			sql.append(", ?");  // 4  postion_id
			sql.append(", ?");  // 5 is_daleted
			sql.append(", ?");  // 6 password
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(")");

			// connectionから別オブジェクトを作成
			ps = connection.prepareStatement(sql.toString());

			//  文字列作成
			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranchId());
			ps.setInt(4, user.getPositionId());
			ps.setInt(5, user.getIsDeleted());
			ps.setString(6, user.getPassword());

			// データベースの更新
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//ユーザー編集
	public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append(" login_id = ?");
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", position_id = ?");
			sql.append(", updated_at = CURRENT_TIMESTAMP");
//			パスワードが空の場合は変更しない
			if (user.getPassword().isEmpty() == false) {
				sql.append(", password = ?");
			}
			sql.append(" WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			if (user.getPassword().isEmpty()) {
				ps.setString(1, user.getLoginId());
				ps.setString(2, user.getName());
				ps.setInt(3, user.getBranchId());
				ps.setInt(4, user.getPositionId());
				ps.setInt(5, user.getId());
			} else {
				ps.setString(1, user.getLoginId());
				ps.setString(2, user.getName());
				ps.setInt(3, user.getBranchId());
				ps.setInt(4, user.getPositionId());
				ps.setString(5, user.getPassword());
				ps.setInt(6, user.getId());

			}

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	// 停止・復活処理
	public void changeIsDeleted(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			String sql = "UPDATE users SET is_deleted = ? WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, user.getIsDeleted());
			ps.setInt(2, user.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	// 特定のユーザーを帰す
	public User getUser(Connection connection, String loginId, String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ? and password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();

			List<User> userList = toUsers(rs, false);

			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
//				検索結果が二個以上
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch( SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	// 特定のユーザーを返す
	public User getUser(Connection connection, String userLoginId) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ?";

			ps = connection.prepareStatement(sql);

			ps.setString(1, userLoginId);

			ResultSet rs = ps.executeQuery();
			List<User> user = toUsers(rs, false);

			if (user.size() == 1) {
				return user.get(0);
			} else if (user.size() >= 2) {
				throw new IllegalStateException("2 <= user.size()");
			} else {
				return null;
			}
		} catch( SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	// 特定のユーザーを返すuserのid
	public User getUser(Connection connection, int userId) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, userId);

			ResultSet rs = ps.executeQuery();
			List<User> user = toUsers(rs, false);

			if (user.size() == 1) {
				return user.get(0);
			} else if (2 <= user.size()) {
//				検索結果が二個以上
				throw new IllegalStateException("2 <= user.size()");
			} else {
				return  null;
			}
		} catch( SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	// すべてのユーザーを返す
	public List<User> getUsers(Connection connection ) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id");
			sql.append(", users.login_id as login_id");
			sql.append(", users.name as name");
			sql.append(", users.is_deleted as is_deleted");
			sql.append(", users.branch_id as branch_id");
			sql.append(", users.position_id as position_id");
			sql.append(", branches.name as branch_name");
			sql.append(", positions.name as position_name");
			sql.append(" FROM ( ");
			sql.append("users INNER JOIN branches ON users.branch_id = branches.id ");
			sql.append(") INNER JOIN positions ON users.position_id = positions.id ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<User> ret = toUsers(rs, true);

			return ret;
		} catch( SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	// 第二引数でtrueだと支店名、部署名をセット
	public List<User> toUsers(ResultSet rs, Boolean isName) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {

				User user = new User();

				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				int branchId = rs.getInt("branch_id");
				int positionId = rs.getInt("position_id");
				int isDeleted = rs.getInt("is_deleted");
				// 支店名などのセット
				if (isName) {
					String branchName = rs.getString("branch_name");
					String positionName = rs.getString("position_name");

					user.setPositionName(positionName);
					user.setBranchName(branchName);
				}

				user.setId(id);
				user.setLoginId(loginId);
				user.setName(name);
				user.setBranchId(branchId);
				user.setPositionId(positionId);
				user.setIsDeleted(isDeleted);

				ret.add(user);
			}

			return ret;
		} finally {
			close(rs);
		}
	}
}
