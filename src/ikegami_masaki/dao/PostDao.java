package ikegami_masaki.dao;

import static ikegami_masaki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import ikegami_masaki.beans.Post;
import ikegami_masaki.exception.SQLRuntimeException;


public class PostDao {

	// メッセージを保存
	public void insert(Connection connection, Post post) {

		PreparedStatement ps = null;
		try {
			// SQL文を作成
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO posts ( ");
			sql.append("user_id");
			sql.append(", subject");
			sql.append(", category");
			sql.append(", text");
			sql.append(", created_at");
			sql.append(", updated_at");
			sql.append(") VALUES (");
			sql.append("?"); // 1 user_id
			sql.append(", ?");  // 2 subject
			sql.append(", ?");  // 3 category
			sql.append(", ?");  // 4 text
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, post.getUserId());
			ps.setString(2, post.getSubject());
			ps.setString(3, post.getCategory());
			ps.setString(4, post.getText());

			// データベースの更新
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	// 投稿を削除
	public int delete(Connection connection, int postId) {

		PreparedStatement ps = null;
		try {
			String sql = "DELETE FROM posts where id = ?";
			ps = connection.prepareStatement(sql);

			ps.setInt(1, postId);

			int ret = ps.executeUpdate();
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}
}