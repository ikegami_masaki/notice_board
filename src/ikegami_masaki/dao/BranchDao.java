package ikegami_masaki.dao;

import static ikegami_masaki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ikegami_masaki.beans.Branch;
import ikegami_masaki.exception.SQLRuntimeException;

public class BranchDao {

		// branchをすべて取得
	public List<Branch>  getAllBranch(Connection connection) {

		PreparedStatement ps = null;
		try {
			// すべてとってくる
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM branches");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Branch> ret = toBranchList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<Branch> toBranchList(ResultSet rs) throws SQLException {

		List<Branch> ret = new ArrayList<Branch>();

		try {
			while(rs.next()) {
				// 検索結果を変換
				int id = rs.getInt("id");
				String name = rs.getString("name");

				// オブジェクトに変換
				Branch branch = new Branch();
				branch.setId(id);
				branch.setName(name);

				ret.add(branch);
			}

			return ret;
		} finally {
			close(rs);
		}
	}
}
