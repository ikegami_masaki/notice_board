package ikegami_masaki.dao;

import static ikegami_masaki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ikegami_masaki.beans.Comment;
import ikegami_masaki.exception.SQLRuntimeException;

public class CommentDao {

	// コメントの保存処理
	public void insert(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append("user_id, ");
			sql.append("post_id, ");
			sql.append("text, ");
			sql.append("created_at, ");
			sql.append("updated_at");
			sql.append(" ) VALUES ( ");
			sql.append("?"); // 1 use_id
			sql.append(", ?");  // 2  post_id
			sql.append(", ?");  // 3  text
			sql.append(", CURRENT_TIMESTAMP"); // created_at
			sql.append(", CURRENT_TIMESTAMP"); // updated_at
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, comment.getUserId());
			ps.setInt(2, comment.getPostId());
			ps.setString(3, comment.getText());
			System.out.println(ps.toString());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public int delete(Connection connection, int commentId) {

		PreparedStatement ps = null;
		try {
			String sql = "DELETE FROM comments WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, commentId);

			int count = ps.executeUpdate();
			return count;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	// コメントをすべて取得
	public List<Comment> getCommnets(Connection connection) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM comments";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			List<Comment> ret = toComment(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	// 検索結果をCommentオブジェクトに変換
	public List<Comment> toComment(ResultSet rs) throws SQLException {

		List<Comment> ret = new ArrayList<Comment>();
		try {
			while (rs.next()) {
				Comment comment = new Comment();
				comment.setId(rs.getInt("id"));
				comment.setPostId(rs.getInt("post_id"));
				comment.setUserId(rs.getInt("user_id"));
				comment.setText(rs.getString("text"));
				comment.setCreatedAt(rs.getTimestamp("created_at"));

				ret.add(comment);
			}
			return ret;
		} finally {
			close(rs);
		}

	}

}
