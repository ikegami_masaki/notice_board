package ikegami_masaki.dao;

import static ikegami_masaki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ikegami_masaki.beans.Position;
import ikegami_masaki.exception.SQLRuntimeException;

public class PositionDao {

	 // すべての部署・役職を取得
	public List<Position> getAllPosition(Connection connection) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM positions");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Position> ret = toPositionList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	// 検索結果からPositionオブジェクトに変換
	public List<Position> toPositionList(ResultSet rs) throws SQLException {

		List<Position> ret = new ArrayList<Position>();

		try {
			while(rs.next()) {
				// 検索結果から情報を取得
				int id = rs.getInt("id");
				String name = rs.getString("name");

				Position position = new Position();
				position.setId(id);
				position.setName(name);

				ret.add(position);
			}

			return ret;
		} finally {
			close(rs);
		}
	}
}
