package ikegami_masaki.service;

import static ikegami_masaki.utils.CloseableUtil.*;
import static ikegami_masaki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import ikegami_masaki.beans.Post;
import ikegami_masaki.beans.UserPost;
import ikegami_masaki.dao.PostDao;
import ikegami_masaki.dao.UserPostDao;

public class PostService {

	public void  register(Post post) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostDao postDao = new PostDao();
			postDao.insert(connection, post);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

//	すべての投稿を返す
	public List<UserPost> getPosts(String category, String startDate, String endDate) {

		Connection connection = null;
		try {
			connection = getConnection();

//			検索条件の文字列を作成


//			投稿を取得
			UserPostDao userPostDao = new UserPostDao();
			List<UserPost> ret = userPostDao.getUserPost(connection, category, startDate, endDate);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

//	投稿削除
	public boolean delete(int postId) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostDao postDao = new PostDao();
			int count = postDao.delete(connection, postId);

			commit(connection);

			if (count == 1) {
				return true;
			} else {
				return false;
			}
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
