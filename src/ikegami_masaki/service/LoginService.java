package ikegami_masaki.service;

import static ikegami_masaki.utils.CloseableUtil.*;
import static ikegami_masaki.utils.DBUtil.*;

import java.sql.Connection;

import ikegami_masaki.beans.User;
import ikegami_masaki.dao.UserDao;
import ikegami_masaki.utils.CipherUtil;

public class LoginService {

//	特定のユーザーを取得
	public User login(String loginId, String password) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(password);

//			ユーザー情報を取得
			UserDao userDao = new UserDao();
			User user = userDao.getUser(connection, loginId, encPassword);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
