package ikegami_masaki.service;

import static ikegami_masaki.utils.CloseableUtil.*;
import static ikegami_masaki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import ikegami_masaki.beans.Position;
import ikegami_masaki.dao.PositionDao;

public class PositionService {

	public List<Position> getAllPosition() {

		Connection connection = null;
		try {
			connection = getConnection();

			PositionDao positionDao = new PositionDao();
			List<Position> ret =  positionDao.getAllPosition(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
