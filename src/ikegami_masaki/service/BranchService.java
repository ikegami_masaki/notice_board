package ikegami_masaki.service;

import static ikegami_masaki.utils.CloseableUtil.*;
import static ikegami_masaki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import ikegami_masaki.beans.Branch;
import ikegami_masaki.dao.BranchDao;

public class BranchService {

	public List<Branch> getAllBranch() {
		Connection connection = null;

		try {

			connection = getConnection();

//			すべてのブランチのリストを取得
			BranchDao branchDao = new BranchDao();
			List<Branch> ret = branchDao.getAllBranch(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
