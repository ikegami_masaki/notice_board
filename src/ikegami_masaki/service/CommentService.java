package ikegami_masaki.service;

import static ikegami_masaki.utils.CloseableUtil.*;
import static ikegami_masaki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import ikegami_masaki.beans.Comment;
import ikegami_masaki.dao.CommentDao;

public class CommentService {


	public void register(Comment comment) {

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.insert(connection, comment);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<Comment> getComments() {

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			List<Comment> ret = commentDao.getCommnets(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

//	コメント削除
	public boolean delete(int commentId) {

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			int count = commentDao.delete(connection, commentId);

			commit(connection);

			if (count == 1) {
				return true;
			} else {
				return false;
			}
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
