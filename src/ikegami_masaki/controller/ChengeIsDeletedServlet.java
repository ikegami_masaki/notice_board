package ikegami_masaki.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ikegami_masaki.beans.User;
import ikegami_masaki.service.UserService;

@WebServlet(urlPatterns = { "/userDelete" })
public class ChengeIsDeletedServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String userIdStr = request.getParameter("userId");

		int userId = Integer.parseInt(userIdStr);
		// 特定のユーザーを取得
		UserService userService = new UserService();
		User user = userService.getUser(userId);

		// 停止処理
		if (user.getIsDeleted() == 0) {
			user.setIsDeleted(1);
			userService.changeIsDeleted(user);
		} else if(user.getIsDeleted() == 1) {
			user.setIsDeleted(0);
			userService.changeIsDeleted(user);
		}

		response.sendRedirect("management");
	}
}
