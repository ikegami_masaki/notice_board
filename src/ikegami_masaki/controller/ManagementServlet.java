package ikegami_masaki.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ikegami_masaki.beans.User;
import ikegami_masaki.service.UserService;

// ユーザー管理画面
@WebServlet(urlPatterns={ "/management" })
public class ManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// ユーザーすべてを取得
		List<User> users = new UserService().getUsers();
		request.setAttribute("users", users);

		request.getRequestDispatcher("management.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
