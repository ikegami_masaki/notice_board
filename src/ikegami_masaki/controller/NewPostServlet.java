package ikegami_masaki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import ikegami_masaki.beans.Post;
import ikegami_masaki.beans.User;
import ikegami_masaki.service.PostService;


//新規投稿のサーブレット
@WebServlet(urlPatterns={ "/newPost" })
public class NewPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	// 投稿画面を表示
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.getRequestDispatcher("newPost.jsp").forward(request, response);
	}

	// 投稿保存の処理
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// ログイン情報を取得
		HttpSession session = request.getSession();
		 List<String> messages = new ArrayList<String>();

		// パラメーターからPostを作成
		Post newPost = getPost(request, session);

		if (isValid(request, messages)) {
			PostService postService = new PostService();
			postService.register(newPost);
			response.sendRedirect("./");
		} else {

			session.setAttribute("errorMessages", messages);
			request.setAttribute("newPost", newPost);
			request.getRequestDispatcher("newPost.jsp").forward(request, response);
		}



	}

	// リクエストからPostオブジェクトを作成
	private Post getPost(HttpServletRequest request, HttpSession session) {

		// セッションからログインユーザー情報を取得
		User loginUser = (User) session.getAttribute("loginUser");

		Post post = new Post();
		post.setUserId(loginUser.getId());
		post.setSubject(request.getParameter("subject"));
		post.setCategory(request.getParameter("category"));
		post.setText(request.getParameter("text"));


		return post;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String subject = request.getParameter("subject");
		String category = request.getParameter("category");
		String text = request.getParameter("text");

		// 件名 30文字以内
		if (StringUtils.isBlank(subject)) {
			messages.add("件名を入力してください");
		} else if (subject.length() > 30) {
			messages.add("件名を30文字以内で入力してください");
		}

		// カテゴリー 10文字以内
		if (StringUtils.isBlank(category)) {
			messages.add("カテゴリーを入力してください");
		} else if (category.length() > 10) {
			messages.add("カテゴリーを10文字以内で入力してください");
		}

		// 本文 1000文字以内
		if (StringUtils.isBlank(text)) {
			messages.add("本文を入力してください");
		} else if (text.length() > 1000) {
			messages.add("本文を1000文字以内で入力してください");
		}

		if (messages.size() >= 1) {
			return false;
		} else {
			return true;
		}

	}

}
