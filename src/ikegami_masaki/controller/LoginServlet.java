package ikegami_masaki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ikegami_masaki.beans.User;
import ikegami_masaki.service.LoginService;


@WebServlet(urlPatterns={"/login"})
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	// ログイン画面を表示
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	// ログイン処理
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッション情報を取得
		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		// パラメーターを取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		// データベース処理（ユーザーを取得）
		LoginService loginService = new LoginService();
		User user = loginService.login(loginId, password);

		if (user != null) {

			// ユーザーの停止状況確認
			if (user.getIsDeleted() == 0) {

				//エラーメッセージを削除
				session.setAttribute("errorMessages", null);
				session.setAttribute("loginUser", user);
				response.sendRedirect("./");
			} else if (user.getIsDeleted() == 1) {

				messages.add("ログインに失敗しました。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("loginId", loginId);
				request.getRequestDispatcher("login.jsp").forward(request, response);
			}
		} else {

			// ログインに失敗したとき
			messages.add("ログインに失敗しました。");
			session.setAttribute("errorMessages", messages);
			request.setAttribute("loginId", loginId);
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}
	}

}
