package ikegami_masaki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import ikegami_masaki.beans.Branch;
import ikegami_masaki.beans.Position;
import ikegami_masaki.beans.User;
import ikegami_masaki.service.BranchService;
import ikegami_masaki.service.PositionService;
import ikegami_masaki.service.UserService;

//新規ユーザー登録処理
@WebServlet(urlPatterns={ "/signup" } )
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	// 新規登録画面の表示
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// フォームに使うものを取得
		List<Branch> branchList = new BranchService().getAllBranch();
		List<Position> positionList = new PositionService().getAllPosition();

		request.setAttribute("branches", branchList);
		request.setAttribute("positions", positionList);

		request.getRequestDispatcher("signup.jsp" ).forward(request, response);
	}


	// 新規登録処理
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// エラーメッセージ用
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		User signupUser = getUser(request);


		//値の確認
		if (isValid(request, messages)) {

			UserService.register(signupUser);
			response.sendRedirect("management");
		} else {

			request.setAttribute("signupUser", signupUser);

			// フォームに使うものを取得
			List<Branch> branchList = new BranchService().getAllBranch();
			List<Position> positionList = new PositionService().getAllPosition();

			request.setAttribute("branches", branchList);
			request.setAttribute("positions", positionList);

			session.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}



	// パラメーターの内容からUserオブジェクトを作成
	private User getUser(HttpServletRequest request) {

		User user = new User();

		user.setLoginId(request.getParameter("loginId"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setPositionId(Integer.parseInt(request.getParameter("positionId")));
		user.setPassword(request.getParameter("password"));

		return user;
	}

	// リクエストから値のチェック
	private boolean isValid(HttpServletRequest request, List<String> messages) {

		// 値の取得
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String confirmtion = request.getParameter("confirmtion");
		int branchId = Integer.parseInt(request.getParameter("branchId"));
		int positionId = Integer.parseInt(request.getParameter("positionId"));

		// 値の確認
		// 半角英数字6～20
		// ログインIDの重複チェック
		User user = new UserService().getUser(loginId);
		if (StringUtils.isBlank(loginId)) {
			messages.add("ログインIDを入力してください");
		} else if (loginId.matches("[a-zA-Z0-9]{6,20}") == false){
			messages.add("ログインIDを半角英数字6～20文字で入力してください");
		} else if (user != null) {
			messages.add("入力されたログインIDは使われています");
		}

		// 10文字以内
		if (StringUtils.isBlank(name)) {
			messages.add("名前を入力してください");
		} else if (name.length() > 10) {
			messages.add("名前を10文字以下で入力してください");
		}

		// パスワードが空の場合
		if (StringUtils.isBlank(password)) {
			messages.add("パスワードを入力してください");
		}

		// パスワードが確認用と同じ
		// パスワードが入力されているときのみ
		if (StringUtils.isBlank(password) == false) {

			 if (password.equals(confirmtion) == false) {

				messages.add("パスワードと確認用パスワードが違います");
			} else if (password.matches("[-_@+*;:#$%&a-zA-Z0-9]{6,20}") == false ) {

				messages.add("記号を含む半角文字6～20文字で入力してください");
			}

		// 確認用のパスワードが空
		} else if (StringUtils.isBlank(confirmtion)) {

			messages.add("確認用パスワードを入力してください");
		}

		// 本社を選択した場合
		// 総務部、情報管理部のみに関連付け
		if (branchId == 1) {
			if (positionId != 1 && positionId != 2) {
				messages.add("本社は総務部または情報管理部で設定してください");
			}
		}

		// 本社以外を選択した場合
		// 支店長または社員のみに関連付け
		if (branchId != 1) {
			if (positionId == 1 || positionId == 2) {
				messages.add("支店は支店長または社員で設定してください");
			}
		}

		if (messages.size() >= 1) {
			return false;
		} else {
			return true;
		}
	}

}
