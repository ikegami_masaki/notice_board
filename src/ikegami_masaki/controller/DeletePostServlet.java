package ikegami_masaki.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ikegami_masaki.service.PostService;

@WebServlet(urlPatterns={ "/deletePost" })
public class DeletePostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//削除する投稿IDを取得
		String postIdStr = request.getParameter("postId");

		int postId = Integer.parseInt(postIdStr);
		new PostService().delete(postId);

		response.sendRedirect("./");
	}
}