package ikegami_masaki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import ikegami_masaki.beans.Branch;
import ikegami_masaki.beans.Position;
import ikegami_masaki.beans.User;
import ikegami_masaki.exception.NoRowsUpdatedRuntimeException;
import ikegami_masaki.service.BranchService;
import ikegami_masaki.service.PositionService;
import ikegami_masaki.service.UserService;


@WebServlet(urlPatterns={ "/userEdit" })
public class UserEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String userIdStr = request.getParameter("userId");
		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		// クエリがない場合の処理
		if (userIdStr == null ) {
			messages.add("不正なパラメーターです");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
			return;
		}
		// 数字以外の場合
		if (userIdStr.matches("[0-9]{1,}") == false) {
			messages.add("不正なパラメーターです");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
			return;
		}

		int userId = Integer.parseInt(userIdStr);

		// 編集対象のユーザー
		User editUser = new UserService().getUser(userId);

		//  対象のユーザーが検索できたかを確認
		if (editUser != null) {
			request.setAttribute("editUser", editUser);

			// フォームに使うものを取得
			List<Branch> branchList = new BranchService().getAllBranch();
			List<Position> positionList = new PositionService().getAllPosition();

			request.setAttribute("branches", branchList);
			request.setAttribute("positions", positionList);

			request.getRequestDispatcher("userEdit.jsp").forward(request, response);
		} else {

			// エラーメッセージをセット
			messages.add("不正なパラメーターです");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
			return;
		}


	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User user = toEditUser(request);

		try {

			if (isValid(request, messages)){

				new UserService().update(user);
				response.sendRedirect("management");
			} else {

				session.setAttribute("errorMessages", messages);
				// フォームに使うものを取得
				List<Branch> branchList = new BranchService().getAllBranch();
				List<Position> positionList = new PositionService().getAllPosition();

				request.setAttribute("branches", branchList);
				request.setAttribute("positions", positionList);
				request.setAttribute("editUser", user);
				request.getRequestDispatcher("userEdit.jsp").forward(request, response);
			}

		} catch (NoRowsUpdatedRuntimeException e) {

			messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
			session.setAttribute("errorMessages", messages);
			// フォームに使うものを取得
			List<Branch> branchList = new BranchService().getAllBranch();
			List<Position> positionList = new PositionService().getAllPosition();

			request.setAttribute("branches", branchList);
			request.setAttribute("positions", positionList);
			request.setAttribute("editUser", user);
			request.getRequestDispatcher("userEdit.jsp").forward(request, response);
			return;
		}


	}

	// リクエストからユーザーを作成
	private User toEditUser(HttpServletRequest request) {

		int id = Integer.parseInt(request.getParameter("userId"));
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		int branchId = Integer.parseInt(request.getParameter("branchId"));
		int positionId = Integer.parseInt(request.getParameter("positionId"));



		User user = new User();
		user.setId(id);
		user.setLoginId(loginId);
		user.setPassword(password);
		user.setName(name);
		user.setBranchId(branchId);
		user.setPositionId(positionId);

		return user;
	}

	// リクエストから値のチェック
	private boolean isValid(HttpServletRequest request, List<String> messages) {

		// 値の取得
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String confirmtion = request.getParameter("confirmtion");
		int branchId = Integer.parseInt(request.getParameter("branchId"));
		int positionId = Integer.parseInt(request.getParameter("positionId"));

		// 値の確認
		// 半角英数字6～20
		// ログインIDの重複チェック
		User user = new UserService().getUser(loginId);
		if (StringUtils.isBlank(loginId)) {
			messages.add("ログインIDを入力してください");
		} else if (loginId.matches("[a-zA-Z0-9]{6,20}") == false){
			messages.add("ログインIDを半角英数字6～20文字で入力してください");
		} else if (user != null) {
			messages.add("入力されたログインIDは使われています");
		}

		// 10文字以内
		if (StringUtils.isBlank(name)) {
			messages.add("名前を入力してください");
		} else if (name.length() > 10) {
			messages.add("名前を10文字以下で入力してください");
		}

		// パスワードが確認用と同じ
		// パスワードが入力されているときのみ
		if (StringUtils.isBlank(password) == false) {

			 if (password.equals(confirmtion) == false) {

				messages.add("パスワードと確認用パスワードが違います");
			} else if (password.matches("[-_@+*;:#$%&a-zA-Z0-9]{6,20}") == false ) {

				messages.add("記号を含む半角文字6～20文字で入力してください");
			} else if (StringUtils.isBlank(confirmtion)) {

				messages.add("確認用パスワードを入力してください");
			}
		}

		// 本社を選択した場合
		// 総務部、情報管理部のみに関連付け
		if (branchId == 1) {
			if (positionId != 1 && positionId != 2) {
				messages.add("本社は総務部または情報管理部で設定してください");
			}
		}

		// 本社以外を選択した場合
		// 支店長または社員のみに関連付け
		if (branchId != 1) {
			if (positionId == 1 || positionId == 2) {
				messages.add("支店は支店長または社員で設定してください");
			}
		}


		if (messages.size() >= 1) {
			return false;
		} else {
			return true;
		}
	}

}
