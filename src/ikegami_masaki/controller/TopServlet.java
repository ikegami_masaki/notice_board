package ikegami_masaki.controller;

import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ikegami_masaki.beans.Comment;
import ikegami_masaki.beans.UserPost;
import ikegami_masaki.service.CommentService;
import ikegami_masaki.service.PostService;

//トップ画面
@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// 検索の初期値を設定
		String category = request.getParameter("category");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");

		// 値保持のため
		request.setAttribute("category", category);
		request.setAttribute("startDate", startDate);
		request.setAttribute("endDate", endDate);

		// 時間の初期値を設定
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		if (startDate == null || startDate.isEmpty()) {
			Date date = Date.valueOf("2018-08-01");
			startDate = date.toString() + " 00:00:00";
		}
		if (endDate == null || endDate.isEmpty()) {
			java.util.Date date = new java.util.Date();request.setCharacterEncoding("UTF-8");
			String dateStr = sdf.format(date);
			endDate = dateStr + " 23:59:59";
		} else {
			endDate += "23:59:59";
		}

		// 投稿をすべて取得
		List<UserPost> userPosts = new PostService().getPosts(category, startDate, endDate);
		request.setAttribute("userPosts", userPosts);

		// コメントをすべて取得
		List<Comment> comments = new CommentService().getComments();
		request.setAttribute("comments", comments);

		request.getRequestDispatcher("top.jsp").forward(request, response);
	}
}
