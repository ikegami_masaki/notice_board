package ikegami_masaki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import ikegami_masaki.beans.Comment;
import ikegami_masaki.beans.User;
import ikegami_masaki.service.CommentService;


@WebServlet(urlPatterns={ "/comment" })
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		Comment comment = toComment(request, session);

		if (isValid(request, messages)) {

			// データベースに保存
			new CommentService().register(comment);
			response.sendRedirect("./");
		} else {

			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
		}
	}

	// リクエストからオブジェクトを生成
	private Comment toComment(HttpServletRequest request, HttpSession session) {

		// ログイン中のユーザーを取得
		User loginUser = (User) session.getAttribute("loginUser");

		Comment comment = new Comment();

		comment.setText(request.getParameter("text"));
		comment.setUserId(loginUser.getId());
		comment.setPostId(Integer.parseInt(request.getParameter("postId")));

		return comment;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String text = request.getParameter("text");

		// 値チェック
		if (StringUtils.isBlank(text)) {
			messages.add("コメントを入力してください");
		} else if (text.length() > 500) {
			messages.add("コメントは500文字以内で入力してください");
		}

		if (messages.size() >= 1) {
			return false;
		} else {
			return true;
		}
	}

}
