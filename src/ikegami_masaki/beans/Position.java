package ikegami_masaki.beans;

import java.io.Serializable;

public class Position implements Serializable {
	private int id;
	// 1 総務部
	// 2 情報管理部
	// 3 支店長
	private String name;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
