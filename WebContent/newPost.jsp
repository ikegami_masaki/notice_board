<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="./css/style.css" type="text/css">
		<title>Insert title here</title>
	</head>
<body>

	<header>
		<div class="header-contents">
			<a href="./" class="logo">LOGO</a>

			<ul class="clearfix">
				<li style="font-size: 17px;"><c:out value="${ loginUser.name }がログイン中"></c:out></li>
				<li><a href="newPost">新規投稿</a></li>
				<!-- 総務部だけに制限 -->
				<c:if test="${ loginUser.positionId == 1 }">
					<li>
						<a href="management">ユーザー管理</a>
					</li>
				</c:if>
				<li><a href="logout">ログアウト</a></li>

			</ul>
		</div>
	</header>

	<div class="form-contents">

		<h2>新規投稿</h2>

		<c:if test="${ not empty errorMessages }">
			<div class="error-messages">
				<ul>
					<c:forEach items="${errorMessages }" var="message">
						<li><c:out value="${ message }" /></li>
					</c:forEach>
					<c:remove var="errorMessages" scope="session" />
				</ul>
			</div>
		</c:if>

		<form action="newPost" method="post">
			<!-- 件名 -->
			<div class="item">
				<label for="subject">件名(30文字以内)</label>
				<input name="subject" id="subject" class="text-form" value="${ newPost.subject }">
			</div>

			<!-- カテゴリ -->
			<div class="item">
				<label for="category">カテゴリー(10文字以内)</label>
				<input name="category" id="caategory" class="text-form" value="${ newPost.category }">
			</div>

			<!-- 本文 -->
			<div class="item">
				<label for="text">本文(1000文字以内)</label>
				<textarea name="text" id="text" class="text-form textarea" cols="35" rows="6">${ newPost.text }</textarea>
			</div>

			<!-- 送信 -->
			<input type="submit" value="送信" class="submit">

		</form>
	</div>

</body>
</html>