<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="./css/style.css" type="text/css">
	<title>ユーザー管理</title>
</head>

<body>

	<header>
		<div class="header-contents">
			<a href="./" class="logo">LOGO</a>

			<ul class="clearfix">
				<li style="font-size: 17px;"><c:out value="${ loginUser.name }がログイン中"></c:out></li>
				<li><a href="newPost">新規投稿</a></li>
				<!-- 総務部だけに制限 -->
				<c:if test="${ loginUser.positionId == 1 }">
					<li>
						<a href="management">ユーザー管理</a>
					</li>
				</c:if>
				<li><a href="logout">ログアウト</a></li>

			</ul>
		</div>
	</header>

	<!-- エラーメッセージ -->
	<c:if test="${ not empty errorMessages }">
		<div class="top-errors">
			<div class="error-messages">
				<ul>
					<c:forEach items="${errorMessages }" var="message">
						<li><c:out value="${ message }" /></li>
					</c:forEach>
					<c:remove var="errorMessages" scope="session" />
				</ul>
			</div>
		</div>
	</c:if>

	<div class="main-contents">

		<h2>ユーザー管理</h2>

		<div class="management-users">

			<table>
				<thead>
					<tr>
						<th>id</th>
						<th>ログインid</th>
						<th>名前</th>
						<th>支店</th>
						<th>部署・役職</th>
						<th>復活・復活</th>
						<th>編集</th>
					</tr>
				</thead>

				<tbody>
					<c:forEach items="${ users }" var="user">
						<tr>
							<td><c:out value="${ user.id }" /></td>
							<td><c:out value="${ user.loginId }" /></td>
							<td><c:out value="${ user.name }" /></td>
							<td><c:out value="${ user.branchName }" /></td>
							<td><c:out value="${ user.positionName }" /></td>
							<td style="text-align: center;">
								<!-- ログイン中のユーザーの復活・復活を制限 -->
								<c:if test="${ loginUser.id != user.id }">
									<form action="userDelete" method="post">
										<input type="hidden" name="userId" value="${ user.id }">
										<c:if test="${ user.isDeleted == 0 }">
											<input type="submit" value="停止" class="user-delete-btn" onclick="return confirm('ユーザーの停止しますか?')">
										</c:if>
										<c:if test="${ user.isDeleted == 1 }">
											<input type="submit" value="復活" style="background-color: #fa8072;" class="user-delete-btn" onclick="return confirm('ユーザーを復活しますか？')">
										</c:if>
									</form>
								</c:if>
								<!-- ログイン中のユーザーはテキストを表示 -->
								<c:if test="${ loginUser.id == user.id}">
									ログイン中
								</c:if>
							</td>
							<td class="user-edit-btn"><a href="userEdit?userId=${ user.id }">編集</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>

		<div class="management-link-list">

			<h3>管理メニュー</h3>

			<nav>
				<ul>
					<li class="signup-link"><a href="signup">　新規ユーザー登録</a></li>
				</ul>
			</nav>
		</div>
	</div>
</body>
</html>