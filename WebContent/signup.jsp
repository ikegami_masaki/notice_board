<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="./css/style.css" type="text/css">
		<title>新規ユーザー登録</title>
	</head>
<body>

	<header>
		<div class="header-contents">
			<a href="./" class="logo">LOGO</a>

			<ul class="clearfix">
				<li style="font-size: 17px;"><c:out value="${ loginUser.name }がログイン中"></c:out></li>
				<li><a href="newPost">新規投稿</a></li>
				<!-- 総務部だけに制限 -->
				<c:if test="${ loginUser.positionId == 1 }">
					<li>
						<a href="management">ユーザー管理</a>
					</li>
				</c:if>
				<li><a href="logout">ログアウト</a></li>

			</ul>
		</div>
	</header>

	<div class="form-contents">

		<h2>新規ユーザー登録</h2>

		<c:if test="${ not empty errorMessages }">
			<div class="error-messages">
				<ul>
					<c:forEach items="${errorMessages }" var="message">
						<li><c:out value="${ message }" /></li>
					</c:forEach>
					<c:remove var="errorMessages" scope="session" />
				</ul>
			</div>
		</c:if>

		<form action="signup" method="post">
			<!-- ユーザーID -->
			<input type="hidden" name="userId" class="text-form" value="${ signupUser.id }">

			<!-- ログインID -->
			<div class="item">
				<label for="loginId">ログインID(半角英数字)</label>
				<input name="loginId" id="loginId" class="text-form" value="${ signupUser.loginId }">
			</div>

			<!-- 名前 -->
			<div class="item">
				<label for="name">名前(10文字以下)</label>
				<input name="name" id="name" class="text-form" value="${ signupUser.name }">
			</div>

			<!-- パスワード -->
			<div class="item">
				<label for="password">パスワード(記号を含む全ての半角文字で6文字以上20文字)</label>
				<input type="password" class="text-form" name="password" id="password">
			</div>

			<!-- パスワード確認用 -->
			<div class="item">
				<label for="confirmtion">パスワード (確認用)</label>
				<input type="password" class="text-form" name="confirmtion" id="confirmtion">
			</div>

			<!-- 自分の支店、役職は変更不可能 -->
			<!-- 支店一覧 -->
			<div class="item">
				<label for="branchId">支店</label>
				<select name="branchId">
					<c:forEach items="${ branches }" var="branch" >
						<!-- 現在の設定中のを初期値に -->
						<option value="${branch.id }"
							<c:if test="${ signupUser.branchId == branch.id }">selected</c:if>>
							<c:out value="${branch.name }" />
						</option>
					</c:forEach>
				</select>
				</div>

			<!-- 部署・役職一覧 -->
			<div class="item">
				<label for="positionId">部署・役職</label>
				<select name="positionId">
					<c:forEach items="${ positions }" var="position">
						<option value="${position.id }"
							<c:if test="${ signupUser.positionId == position.id }">selected</c:if>>
							<c:out value="${ position.name }" />
						</option>
					</c:forEach>
				</select>
			</div>

			<input type="submit" class="submit" value="登録">
		</form>

	</div>
</body>
</html>