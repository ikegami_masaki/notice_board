<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="./css/style.css" type="text/css">
		<title>トップページ</title>
	</head>
<body>
	<header>
		<div class="header-contents">
			<a href="./" class="logo">LOGO</a>

			<ul class="clearfix">
				<li style="font-size: 17px;"><c:out value="${ loginUser.name }がログイン中"></c:out></li>
				<li><a href="newPost">新規投稿</a></li>
				<!-- 総務部だけに制限 -->
				<c:if test="${ loginUser.positionId == 1 }">
					<li>
						<a href="management">ユーザー管理</a>
					</li>
				</c:if>
				<li><a href="logout">ログアウト</a></li>

			</ul>
		</div>
	</header>

	<!-- エラーメッセージ -->
	<c:if test="${ not empty errorMessages }">
		<div class="top-errors">
			<div class="error-messages">
				<ul>
					<c:forEach items="${errorMessages }" var="message">
						<li><c:out value="${ message }" /></li>
					</c:forEach>
					<c:remove var="errorMessages" scope="session" />
				</ul>
			</div>
		</div>
	</c:if>

	<div class="main-contents">

		<!-- 画面左配置 -->
		<!-- ユーザー情報 -->
		<div class="top-left">
			<div class="login-user">
				<div class="user-info">
					<h3>ユーザー情報</h3>
					<p>ID：<c:out value="${ loginUser.loginId }" /></p>
					<p>名前：<c:out value="${ loginUser.name }" /></p>
				</div>
			</div>
		</div>

		<!-- 画面中央配置 -->
		<div class="top-center">

			<h3>投稿一覧</h3>

			<div class="post-contents">
				<c:forEach items="${ userPosts }" var="post">
					<div class="post">
					<!-- 投稿の削除をログインユーザーに制限 -->
							<c:if test="${ loginUser.id == post.userId }">
								<form action="deletePost" method="post">
									<input type="hidden" name="postId" value="${post.id }">
									<input type="submit" value="投稿削除" class="post-delete-btn" onclick="return confirm('投稿を削除しますか?')">
								</form>
							</c:if>
						<div class="post-data">
							<!-- 投稿の表示 -->
							<p class="title"><span>＜投稿者＞　　 </span> <c:out value="${ post.userName }" /></p>
							<p class="title"><span>＜カテゴリー＞ </span> <c:out value="${ post.category}" /></p>

							<p class="title"><span>＜件名＞　　　 </span> <c:out value="${ post.subject }" /></p>

							<p class="title"><span>＜本文＞</span></p>
							<pre class="text"><c:out value="${ post.text }" /></pre>
							<div class="clearfix">
								<p class="post-date"> [<c:out value="${ post.createdAt }" />]</p>
							</div>
						</div>

						<div class="comment">
							<div class="comment-form">
								<form action="comment" method="post" class="clearfix">
									<!-- 本文 -->
									<textarea name="text" id="text" class="text item"></textarea>

									<!-- 投稿番号を設定 -->
									<input type="hidden" name="postId" value="${ post.id }">
									<input  type="submit" class="submit" value="コメント投稿">
								</form>
							</div>

							<div class="comment-list">

								<h4  style="font-size: 15px;">コメント一覧</h4>

								<ul>
									<c:forEach items="${ comments }" var="comment">
										<c:if test="${comment.postId == post.id }">
											<li>
												<pre class="text"><c:out value="${ comment.text}" /></pre>

												<!-- コメントの削除をログインユーザーに制限 -->
												<c:if test="${ loginUser.id == comment.userId }">
													<form action="deleteComment" method="post" >
														<input type="hidden" name="commentId" value="${ comment.id }" >
														<input type="submit" class="delete-btn" value="コメント削除" onclick="return confirm('コメントを削除しますか？')">
													</form>
												</c:if>
											</li>
										</c:if>
									</c:forEach>
								</ul>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>

		<!-- 画面右側 -->
		<div class="top-right">
			<div class="post-search">

				<h3>投稿検索</h3>

				<form action="./">
					<!-- カテゴリー -->
					<div class="category-search">
						<label for="category"><strong>カテゴリー</strong></label> <br />
						<input name="category" id="category" value="${ category }"> <br />
					</div>

					<!-- 作成日 -->
					<div class="date-search">
						<label for="startDate"><strong>投稿日</strong></label> <br />
						<input type="date" name="startDate" id="startDate" value="${ startDate }"> <br />
						～
						<!-- 作成日 -->
						<input type="date" name="endDate" id="endDate" value="${ endDate }"> <br />
					</div>

					<input type="submit" value="検索" class="search-btn">
				</form>
			</div>
		</div>
	</div>


</body>
</html>