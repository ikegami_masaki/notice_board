<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="./css/style.css" type="text/css">
		<title>ログイン</title>
	</head>
<body>

	<div class="form-contents">

		<h2>ログイン</h2>

		<c:if test="${ not empty errorMessages }">
			<div class="error-messages">
				<ul>
					<c:forEach items="${errorMessages }" var="message">
						<li><c:out value="${ message }" /></li>
					</c:forEach>
					<c:remove var="errorMessages" scope="session" />
				</ul>
			</div>
		</c:if>

		<form action="login" method="post">
			<!-- ログインID -->
			<div class="item">
				<label for="loginId">ログインID</label>
				<input name="loginId"  id="loginId" class="text-form" value="${ loginId }">
			</div>

			<!-- パスワード -->
			<div class="item">
				<label for="password">パスワード</label>
				<input type="password" name="password" class="text-form" id="password">
			</div>

			<!-- 送信 -->
			<input type="submit" class="submit" value="ログイン">

		</form>
	</div>

</body>
</html>